﻿using UnityEngine;
using System.Collections; 
using UnityEngine.UI;

public class Door : MonoBehaviour {
	public int levelToLoad;
	private gameMaster gm;

	void Start () {
		gm = GameObject.FindGameObjectWithTag("GameMaster").GetComponent<gameMaster> ();
	}
	

	void OnTriggerEnter2D(Collider2D col){
		
		if (col.CompareTag("Body")){
			gm.InputText.text = ("[E] to Enter");
			if (Input.GetKeyDown ("e")) {

				Application.LoadLevel (levelToLoad);

			}
		}
	}

	void OnTriggerStay2D(Collider2D col){
		if (col.CompareTag ("Body")) {
			if (Input.GetKeyDown ("e")) {
			
				Application.LoadLevel (levelToLoad);
			
			}
		}
	}

	void OnTriggerExit2D(Collider2D col){
	
		if (col.CompareTag ("Body")) {
			gm.InputText.text = ("");
		
		}
	
	}
}
