﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Seeker))]

public class EnemyAI2 : MonoBehaviour {
	public Transform target;
	public float updateRate = 2f;

	private Seeker seeker;
	private Rigidbody2D rb;

	public float speed = 300f;
	public ForceMode2D fMode;
	private Animator anim;
	private bool move = false;

	[HideInInspector]
	public bool pathIsEnded = false;

	public float nextWaypointDistance = 3;

	private int currentWaypoint = 0;

	void Start(){
		rb = GetComponent<Rigidbody2D> ();
		anim = gameObject.GetComponent<Animator> ();
		if (target == null) {
			return;
		}
		//seeker.StartPath (transform.position, target.position, OnPathComplete);

		//StartCoroutine (UpdatePath ());
	}

	void FixedUpdate()
	{
		if (target == null) {
			return;
		}

		//Debug.Log ("Has target");
	    Vector2 dir = new Vector2(target.position.x, target.position.y) - rb.position;

        if (Mathf.Abs(dir.x) < 80f)
        {
			move = true;
            //Debug.Log(dir.x);
            dir.Normalize();
            dir.y = 0;
            dir *= speed * Time.fixedDeltaTime;

            //rb.AddForce(dir, ForceMode2D.Impulse);
			rb.velocity = dir;
        }

		anim.SetBool("Move", move);

        /*
        if (dir.x > 0)
        {
            Vector3 scale = transform.localScale;
            scale.x = -1;
            transform.localScale = scale;
        }
        else if (dir.x < 0)
        {
            Vector3 scale = transform.localScale;
            scale.x = 1;
            transform.localScale = scale;
        }*/
	
	}
}
