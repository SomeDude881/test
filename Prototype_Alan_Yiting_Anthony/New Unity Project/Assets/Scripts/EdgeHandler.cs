﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;

/*
 * Created by Alan, 12/07/16
 * 
 * This script is designed to affect the player game object (body).
 * Allowing the player to:
 * may switch to colliders
 *  - detect if they are close to a ledge (using raycasters) for general use.
 *      - 1 ray caster should be positioned near the hand of the jump animation
 *          detects if air
 *  Should check using this 1st to save resources from checking the above
 *      - 1 ray caster should be placed about a head size downwards
 *          detects if a wall
 *      - air should be null, wall should collide to initiate edge grabbing.
 *      needs to place player position at the closest corner. (How to decide?)
 *          use the centre, add half height, but decide which corner
 *  - puts the player in a edge grabbing state in the animation state machine.
 *      - edgeGrab = true
 *      - reduces the player's controls into 2 options. Not sure how to do that. (Set to KI?)
 *          uh how to detect forward? check the transform scale silly
 *          - forward/up -> player climbs up the edge. (plays an animation to climb, sets player position to new one.)
 *              edgeGrab = false, edgeClimb = true (set to false afterwards)
 *          - back/down -> player lets go of edge and falls (players the jump animation/ free fall animation)
 *              edgeGrab = false, inAir(?) = true
 *      Note need to edit code to only apply movement during a certain state. IdleStae/SceneState - for conversations, prevent moving    and for the edge grab state
 */

public class EdgeHandler : MonoBehaviour {

    public bool ehAirTriggered;  //check if the top edgeTrigger has a collision
    public bool ehWallTriggered;  //check if the bottom edgeTrigger has a collision
    private bool ehOnEdge;  //checks if player has been moved to hold onto  the ledge
    private bool ehIsClimbEdge; //checks if the player input is to climb the edge
    private bool ehIsDropEdge; //if the player is dropping from the ledge
    public Vector2 ehEdgePosition;  //the position of the edge, not the position of the collider for it.

    private Animator ehAnimator;    //the animation state, used to change the state and read from it
    private Rigidbody2D ehRB2D; //the rigidbody2d for the player
    private BoxCollider2D ehBodyCol;    //collider for player

	// Use this for initialization
	void Start () {
        ehAnimator = GetComponent<Animator>();
        ehRB2D = GetComponent<Rigidbody2D>();
        ehBodyCol = GetComponent<BoxCollider2D>();

        ehAirTriggered = false;
        ehWallTriggered = false;
        ehEdgePosition = new Vector2(0,0);

        ehIsClimbEdge = false;
        ehIsDropEdge = false;
    }
	
    //hmm, should check each frame? or have a collider specifically for this?
	// Update is called once per frame
	void Update () {
	    if(ehAnimator.GetBool("GrabEdge"))
        {   //has already grab the edge

            if (!ehOnEdge)
            {
                //now needs to set the player position to look like they are hanging from the edge
                Vector3 move2Edge = new Vector3(ehEdgePosition.x - transform.position.x, ehEdgePosition.y - transform.position.y, 0);
                if (move2Edge.magnitude > 0.2f)
                    transform.position += 4f * Time.deltaTime * move2Edge;
                else
                {
                    //note may need to check if there is enough room to climb on. (have a collider above the edge?)
                    ehRB2D.velocity = new Vector2(0f, 0f);
                    ehOnEdge = true;    //should be close enough to the edge

                    ehIsClimbEdge = false;
                    ehIsDropEdge = false;
                }
            }
            else if (!ehAnimator.GetBool("Climb"))  //not climbing, but hanging from edge
            {
                //handles the input when hanging on ledge
                //here the player can input options to either let go or climb the edge
                if (CrossPlatformInputManager.GetButtonDown("Jump"))
                    ehIsClimbEdge = true;
                else
                {
                    //how about multiplying them! same direction > 0, opposite is <. no input is 0
                    //results in a make shift XOR gate

                    //ensures there is input at the very least
                    if (CrossPlatformInputManager.GetAxis("Horizontal") != 0)
                    {
                        //if there is an input, check to see direction is facing the same way as the scale.z
                        //can be 1 or 0
                        float signDirection = Mathf.Sign(transform.localScale.x) * Mathf.Sign(CrossPlatformInputManager.GetAxis("Horizontal"));

                        if(signDirection > 0)
                        {   //if same
                            ehIsClimbEdge = true;
                        }
                        else
                        {   //if opposite
                            ehIsDropEdge = true;
                        }
                    }
                }

                if (ehIsClimbEdge)
                    ehAnimator.SetTrigger("Climb"); //activates the climb animation
                else if (ehIsDropEdge)
                {
                    ehAnimator.SetBool("GrabEdge", false);//here is when the character will drop from the ledge
                    ehAnimator.SetBool("Ground", false);  //ensures player is in the air animation

                    //reset these values
                    ehAirTriggered = false;
                    ehWallTriggered = false;
                    ehIsClimbEdge = false;

                    //be sure to reset the rigidBody2D to working status
                    ehRB2D.isKinematic = false;
                }
            }

        }
        else
        {   //hasnt grabbed the edge yet            
            if(!ehAnimator.GetBool("Ground"))
            {   //needs to check if in air before checking for edges
                
                //the collision checks are handled within the edgeTrigger Gameobject script
                if(!ehAirTriggered && ehWallTriggered)  //should only be called once, then goes to the above if next frame
                {   //if there is air above the wall, counts as an edge
                    //so here we let the animator know we grabbed it
                    ehAnimator.SetBool("GrabEdge", true);

                    //now adds padding to the location of the edge so that the player will look like they are holding it. The Mathf.Sign is used to determine to displace left or right based on the x scale
                    ehEdgePosition.x += Mathf.Sign(transform.localScale.x) * ehBodyCol.size.x * -0.6f;  //0.6f to give an additional padding due to collider positioning to sprites
                    ehEdgePosition.y += ehBodyCol.size.y * -0.5f;

                    //also needs to set rigidBody2D to kinematic, as prevent gravity
                    ehRB2D.isKinematic = true;  //note the velocity is set to 0 automatically.
                    ehOnEdge = false;
                }
            }
        }
	}

    //during the climb animation, it should call this to revert the rigidBody2D back to normal and set the position of the idle to be above the edge
    public void AfterClimbEdge()
    {
        //we left the ledge, so set it to false
        ehAnimator.SetBool("GrabEdge", false);
        //reset these values
        ehAirTriggered = false;
        ehWallTriggered = false;
        ehIsClimbEdge = false;

        //besure to reset the rigidBody2D to working status
        ehRB2D.isKinematic = false;

        //sets the player position to be exactly where the animation ends, somehow. The Mathf.Sign is used to determine to displace left or right based on the x scale
        transform.position = new Vector2(ehEdgePosition.x + (Mathf.Sign(transform.localScale.x) * ehBodyCol.size.x * 1.6f), ehEdgePosition.y + (ehBodyCol.size.y + GetComponent<CircleCollider2D>().radius));
    }
}
