﻿using UnityEngine;
using System.Collections;

/*
 * Warning: extreme coupling with the script EdgeHandler.cs
 * 
 * This class is meant to be used for 2 types of triggers.
 * The triggers are to activate the booleans: ehAirTriggered and ehWallTriggered in the EdgeHandler.cs
 */
public class EdgeTriggerDetection : MonoBehaviour
{
    public bool etdForAir; //for air or wall
    public GameObject etdPlayer;    //the reference to get the script from
    private EdgeHandler etdEdgeHandler;  //the script to change the variables
    
    public void Start()
    {
        etdEdgeHandler = etdPlayer.GetComponent<EdgeHandler>();
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        //Ash, next time pls, spell Obstacle correctly, could even rename to level.
        //below thanks for letting me check if collider in a certain layer or not
        //http://answers.unity3d.com/questions/261556/how-to-tell-if-my-character-hit-a-collider-of-a-ce.html
        if (collision.gameObject.layer == LayerMask.NameToLayer("Obstcle"))
        { //checks if the collider is part of the level
            if (etdForAir)
            {
                etdEdgeHandler.ehAirTriggered = true;
            }
            else
            {
                etdEdgeHandler.ehWallTriggered = true;
                if(collision.enabled)
                {
                    //here calculates the 1D vector pointing from player to centre, uses this to calculate the closest edge.
                    float dir2Wall = collision.transform.position.x - etdPlayer.transform.position.x;

                    if(dir2Wall > 0)
                    {   //the edge is on the right of the player
                        //thanks to below for a how to get the collider width and height
                        //http://answers.unity3d.com/questions/24012/find-size-of-gameobject.html
                        etdEdgeHandler.ehEdgePosition = new Vector2(collision.transform.position.x - (collision.bounds.size.x * 0.5f), collision.transform.position.y + (collision.bounds.size.y * 0.5f));
                    }
                    else
                    {   //the edge must be on the left of the player, does the opposite direction of the above for the x value
                        etdEdgeHandler.ehEdgePosition = new Vector2(collision.transform.position.x + (collision.bounds.size.x * 0.5f), collision.transform.position.y + (collision.bounds.size.y * 0.5f));
                    }
                }                
            }
        }
    }

    public void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Obstcle"))
        {
            if (etdForAir)
            {
                etdEdgeHandler.ehAirTriggered = false;
            }
            else
            {
                etdEdgeHandler.ehWallTriggered = false;
            }
        }
    }
}
