﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;

public class ShootHand : MonoBehaviour
{

    [SerializeField]
    public GameObject m_hProj; //pointer to gameobject hand that is fired
    public GameObject m_hand;    //the actual hand during animation
    public float launchSpeed; //the launch speed of the hand
    public float returnSpeed;    //the minimum return speed
    public float time2Drop;   //the time(seconds) to travel before falling. (Hand flies straight then drops after this time)
    //time is easier to keep track of instead of distance, cause obstacles can cause issues with distance tracking.

    private float normGrav;  //recalls the current gravity force
    private Rigidbody2D rb;          //the rigid body for physics
    private Collider2D coll;        //to disable when called back
    private float timeTravelled;   //keeps track of time passed since shot.

    //for state purposes
    private bool handShot;   //used to check if the hand is shot or not
    private bool handReturn; //to check if the hand has returned to the player

    // Use this for initialization
    void Start()
    {
        m_hProj.SetActive(false);
        handShot = false;
		handReturn = true;
        rb = m_hProj.GetComponent<Rigidbody2D>();
        rb.position = transform.position;
        coll = m_hProj.GetComponent<Collider2D>();
        timeTravelled = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        //if fire is pressed
        if (CrossPlatformInputManager.GetButtonDown("Fire1"))
        {
            fireHand();
            //Debug.Log(rb.velocity);
        }

        if (!handReturn)
        {
            if (!handShot)
            {   //occurs when hand is returning
                Vector3 flyBack = m_hand.transform.position - m_hProj.transform.position;

                if (flyBack.sqrMagnitude < 50)   //can't get the trigger working, so here is a temp work around
                {
                    m_hProj.SetActive(false);
                    handShot = false;
                    handReturn = true;
                    m_hand.SetActive(true);
                }
                else
                {
                    if (flyBack.magnitude < returnSpeed)
                    {   //if the hand returning flies too slow, set to 5f speed
                        flyBack.Normalize();
                        flyBack *= returnSpeed;
                    }
                    rb.velocity = flyBack;
                }
            }
            else //hand is shot and in air
            {
                //increases the time passed
                timeTravelled += Time.fixedDeltaTime;

                if (timeTravelled < time2Drop)
                {
                    //Debug.Log(rb.velocity);
                }
                else
                {
                    //rotates the hand based on the direction it is flying
                    float zRot = Vector2.Angle(rb.velocity, new Vector2(1, 0));
                    if (zRot > 5f)  //if angle of rotation is bigger than 5 degrees, rotate it.
                    {
                        m_hProj.transform.rotation = Quaternion.Euler(0f, 0f, zRot + 90f);
                    }
                    rb.isKinematic = false;
                }
            }
        }
    }

    void fireHand()
    {
        //when called, will fire the hand
        if (!handShot && handReturn)
        {
            //Debug.Log("Shoot");
            //set hand position at player
            m_hProj.transform.position = m_hand.transform.position;
            m_hand.SetActive(false);
            m_hand.active = false;

            //shoot hand from player position
            m_hProj.SetActive(true);

            //counter starts when shot
            timeTravelled = 0f;
            rb.isKinematic = true;
            coll.enabled = true;
            //update the state
            handShot = true;
            handReturn = false;

            //calcs the direction to shoot at
            Vector3 difference = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
            difference.Normalize();
            difference.z = 0f;

            //angles the hand to the angle of the vector
            float rotZ = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;
            rotZ += 90f;
            m_hProj.transform.rotation = Quaternion.Euler(0f, 0f, rotZ);

            //multiplies the launch speed and applies to the rigidbody
            difference *= launchSpeed;
            rb.velocity = new Vector2(difference.x, difference.y);
            //Debug.Log(rb.velocity);
        }
        else if (!handReturn)
        {   //make hands fly back to player
            //Debug.Log("Return");
            handShot = false;
            coll.enabled = false;
        }
    }
}
