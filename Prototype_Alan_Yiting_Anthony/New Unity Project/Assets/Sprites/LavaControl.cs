﻿using UnityEngine;
using System.Collections;

public class LavaControl : MonoBehaviour {

	private PlayerHP p;
	// Use this for initialization
	void Start () {
		p = GameObject.FindGameObjectWithTag ("Player").GetComponent<PlayerHP> ();
	
	}
	
	// Update is called once per frame
	void OnCollisionEnter2D(Collision2D col){
		if(col.gameObject.tag == "Player"){
			p.Hurt (1);
			StartCoroutine (p.KnockBack (0.02f,8000,p.transform.position));
		}
        
	}
}
