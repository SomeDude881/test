﻿using UnityEngine;
using System.Collections;

public class SimpleEnemyAI : MonoBehaviour {
	public int MaxHealth = 20;
	public int curHealth;
	public Collider2D attackTrigger;
	private Animator anim;
	private bool mHurt = false;
	private bool mmHurt = false;
	private float attackTimer = 0;
	private float attackCd = 1f;
	// Use this for initialization
	void Awake () {
		curHealth = MaxHealth;
		attackTrigger.enabled = true;
		anim = gameObject.GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {

		if (mmHurt == true && !mHurt) {
			attackTimer = attackCd;
			mHurt = true;
			mmHurt = false;

		}

		if (curHealth <= 0) {
		
			Destroy (gameObject);
		}

		if (mHurt) {
			if (attackTimer > 0) {

				attackTimer -= Time.deltaTime;

			} else {
				mHurt = false;
				mmHurt = false;

			}

		}

		//anim.SetBool ("Mhurt", mHurt);
	
	}

	public void Damage(int damage){
		curHealth -= damage;
		mmHurt = true;
	}
}
