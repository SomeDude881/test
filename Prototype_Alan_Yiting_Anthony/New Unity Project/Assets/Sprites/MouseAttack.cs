﻿using UnityEngine;
using System.Collections;

public class MouseAttack : MonoBehaviour {

		public int dmg = 5;
		// Use this for initialization
		void OnTriggerEnter2D(Collider2D col){

			if (col.isTrigger != true && col.CompareTag ("Body")) {
			
				col.SendMessageUpwards ("Hurt", dmg);
			    Debug.LogError ("hit");
			}
		}


}
