using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace UnityStandardAssets._2D
{
    [RequireComponent(typeof (PlatformerCharacter2D))]

    /* use this class as the connection between input and player control.
     *  doing so allows us to control the player responses based on the current state they are in.
    */
    public class Platformer2DUserControl : MonoBehaviour
    {
        private PlatformerCharacter2D m_Character;
        //private EdgeHandler m_EdgeHandler;
        private Animator m_Animator;    //used to get variables that represent the current state of the player
        private bool m_Jump;


        private void Awake()
        {
            m_Character = GetComponent<PlatformerCharacter2D>();
            m_Animator = GetComponent<Animator>();
        }


        private void Update()
        {
            if (!m_Jump)
            {
                // Read the jump input in Update so button presses aren't missed.
                m_Jump = CrossPlatformInputManager.GetButtonDown("Jump");
            }
        }


        private void FixedUpdate()
        {
            // Read the inputs.
            bool crouch = Input.GetKey(KeyCode.LeftControl);
            float h = CrossPlatformInputManager.GetAxis("Horizontal");

            //added by Alan, if player is holding edge, don't move normally, instead let the EdgeHandler.cs handle it
            if (m_Animator.GetBool("GrabEdge"))
            {
                //the code is run in the EdgeHandler code, for a higher cohesive and less coupling based code
            }
            else
            {
                // Pass all parameters to the character control script.
                m_Character.Move(h, crouch, m_Jump);
            }

            m_Jump = false;
        }
    }
}
