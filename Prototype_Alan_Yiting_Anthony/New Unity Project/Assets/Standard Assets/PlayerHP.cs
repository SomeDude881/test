﻿using UnityEngine;
using System.Collections;

public class PlayerHP : MonoBehaviour {

	public int curHealth;
	public int MaxHealth = 100;
	private Rigidbody2D rb2d;
	private bool hurt = false;
	private bool gethurt = false;
	private bool dead = false;
	private float attackTimer = 0;
	private float attackCd = 1f;
	private Animator anim;

	void Start () {
		
		curHealth = MaxHealth;
		rb2d = GetComponent<Rigidbody2D>();
		anim = gameObject.GetComponent<Animator> ();

	}
	
	// Update is called once per frame
	void Update () {
		if (gethurt == true && !hurt) {
			attackTimer = attackCd;
			hurt = true;
			gethurt = false;
		
		}
		if (curHealth > MaxHealth) {
			curHealth = MaxHealth;
		}

		if (curHealth <= 0) {
			dead = true;
			almost ();
		}

		if (hurt) {
			if (attackTimer > 0) {

				attackTimer -= Time.deltaTime;

			} else {
				hurt = false;
				gethurt = false;

			}

		}

		anim.SetBool ("Hurt", hurt);
	}

	public void Hurt(int damage){
		curHealth -= damage;
		gethurt = true;
	}

	public void almost(){
		anim.SetBool ("Dead", dead);
	
	}
	public void Die(){
		Application.LoadLevel (Application.loadedLevel);
	}

	public IEnumerator KnockBack(float knockDur, float knockbackPwr, Vector3 knockbackDir){
	
		float timer = 0;
		while (knockDur > timer) {
			timer += Time.deltaTime;
			rb2d.AddForce(new Vector3 (knockbackDir.x * -100, knockbackDir.y * knockbackPwr, transform.position.z));
		
		}

		yield return 0;
	}
}
